# Copyright © 2017-2018   Thomas Bellman, Linköping, Sweden
# Licensed under the GNU LGPL v3+; see the README file for more information.


/*
 * Rebuild the Grub 2.x config file.
 */
class bootloader::grub2::rebuild_grub_cfg
{
    exec {
	'bootloader::grub2::rebuild_grub_cfg':
	    command => 'if [ -f /etc/grub2-efi.cfg ]; then grub2-mkconfig -o /etc/grub2-efi.cfg; else grub2-mkconfig -o /boot/grub2/grub.cfg; fi',
	    path => ['/bin', '/usr/bin', '/sbin', '/usr/sbin'],
	    provider => 'shell',
	    refreshonly => true;
    }
}
