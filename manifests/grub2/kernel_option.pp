# Copyright © 2017-2022   Thomas Bellman, Linköping, Sweden
# Licensed under the GNU LGPL v3+; see the README file for more information.


/*
 * Internal helper for the bootloader::kernel_option definition.
 */
define bootloader::grub2::kernel_option($ensure, $value)
{
    include bootloader::grub2::rebuild_grub_cfg

    $grub_defaults_file = '/etc/default/grub'
    $cfgparam = $::virtual ? {
	'xen0'  => 'GRUB_CMDLINE_LINUX_XEN_REPLACE_DEFAULT',
	default => 'GRUB_CMDLINE_LINUX',
    }
    $qname = regexp_quote($name)
    # Augeas path expression to find nodes for the kernel parameter
    $pathexpr = "${cfgparam}/value[. =~ regexp('${qname}(=.*)?')]"

    if ($ensure == 'absent')
    {
	augeas {
	    "bootloader::grub2::kernel_option::${name}":
		incl    => $grub_defaults_file,
		lens    => 'Shellvars_list.lns',
		context => "/files${grub_defaults_file}",
		changes => "rm ${pathexpr}",
		notify  => Class['bootloader::grub2::rebuild_grub_cfg'];
	}
    }
    elsif ($ensure != 'present')
    {
	fail("Bootloader::Grub2::Kernel_Option[${title}]: ",
	     "Bad ensure parameter, ${ensure}")
    }
    elsif ($value == true or $value == false)
    {
	augeas {
	    "bootloader::grub2::kernel_option::${name}":
		incl    => $grub_defaults_file,
		lens    => 'Shellvars_list.lns',
		context => "/files${grub_defaults_file}",
		changes => "set ${pathexpr} '${name}'",
		notify  => Class['bootloader::grub2::rebuild_grub_cfg'];
	}
    }
    else
    {
	augeas {
	    "bootloader::grub2::kernel_option::${name}":
		incl    => $grub_defaults_file,
		lens    => 'Shellvars_list.lns',
		context => "/files${grub_defaults_file}",
		changes => "set ${pathexpr} '${name}=${value}'",
		notify  => Class['bootloader::grub2::rebuild_grub_cfg'];
	}
    }
}
