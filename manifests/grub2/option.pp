# Copyright © 2022   Thomas Bellman, Linköping, Sweden
# Licensed under the GNU LGPL v3+; see the README file for more information.


/*
 * Set an option for Grub 2 (in /etc/default/grub).
 *
 * Note that these options are quite different from the ones Grub 0.x,
 * and they actually affect the generation of the /boot/grub2/grub.cfg
 * file, and only indirectly affects the operation of Grub 2 itself.
 *
 * Parameters
 *  - name	Grub2 option to set or remove
 *
 *  - ensure	One of 'present' (the default) or 'absent'.
 *
 *  - value	What value to set for the option.  Note that if the value
 *		needs quoting, e.g. surrounding by double quotes, then
 *		you need to do so yourself!
 */
define bootloader::grub2::option($ensure='present', $value)
{
    include bootloader::grub2::rebuild_grub_cfg

    cfgfile::config_option {
	"bootloader::grub2::option::${name}":
	    file => '/etc/default/grub',
	    option => $name, value => $value,
	    ensure => $ensure,
	    notify => Class['bootloader::grub2::rebuild_grub_cfg'];
    }
}
