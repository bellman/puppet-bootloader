# Copyright © 2017-2018   Thomas Bellman, Linköping, Sweden
# Licensed under the GNU LGPL v3+; see the README file for more information.


/*
 * Set or remove a kernel commandline option.
 *
 * Depending on the details of the bootloader, the option might be set/unset
 * for all kernels, or just for all Linux kernels.  There is currently no
 * way to control this.
 *
 * Parameters:
 *  - name	Kernel commandline option to set or remove.
 *
 *  - ensure	One of 'present' or 'absent'.
 *
 *  - value	The value to set the option to.  If set to true or false,
 *		it will be a "bare" option, i.e. something like "single".
 *		If a string, a valued option will be set, i.e. something
 *		like "console=ttyS0".
 *
 *  - provider	The bootloader in use.  Supported values are 'grub0' and
 *		'grub2'.  If not specified, the bootloader will be guessed
 *		based on the operating system.
 */
define bootloader::kernel_option(
	$ensure='present', $value=undef, $provider=undef)
{
    if ($provider) {
	$bootloader = $provider
    } else {
	include bootloader::guess
	$bootloader = $bootloader::guess::bootloader
    }
    case $bootloader
    {
	'grub0': {
	    bootloader::grub0::kernel_option {
		$title:
		    name => $name, ensure => $ensure, value => $value;
	    }
	}
	'grub2': {
	    bootloader::grub2::kernel_option {
		$title:
		    name => $name, ensure => $ensure, value => $value;
	    }
	}
	default: {
	    fail("Bootloader::Kernel_option[${title}]: ",
		 "Unsupported bootloader, ${bootloader}")
	}
    }
}
