# Copyright © 2017   Thomas Bellman, Linköping, Sweden
# Licensed under the GNU LGPL v3+; see the README file for more information.


/*
 * Try to guess what bootloader is in use, based on the OS.
 * Sets the $bootloader variable to the guess.
 * The heuristics used are not very good...
 * This is intended as an internal helper class, not for end users.
 */
class bootloader::guess
{
    if ($::operatingsystem == 'Fedora' and
	versioncmp($::operatingsystemrelease, '18') >= 0)
    {
	$bootloader = 'grub2'
    }
    elsif ($::operatingsystem == 'Fedora' and
	   versioncmp($::operatingsystemrelease, '15') < 0)
    {
	$bootloader = 'grub0'
    }
    elsif ($::osfamily == 'RedHat'  and
	   versioncmp($::operatingsystemrelease, '7') >= 0)
    {
	$bootloader = 'grub2'
    }
    elsif ($::osfamily == 'RedHat'  and
	   versioncmp($::operatingsystemrelease, '7') < 0)
    {
	$bootloader = 'grub0'
    }
    else
    {
	fail("Cannot guess what bootloader is in use.")
    }
}
