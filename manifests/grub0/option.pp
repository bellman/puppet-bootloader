# Copyright © 2018   Thomas Bellman, Linköping, Sweden
# Licensed under the GNU LGPL v3+; see the README file for more information.


/*
 * Set or remove an option for Grub 0.x.
 *
 * Parameters
 *  - name	Grub0 option to set or remove
 *
 *  - ensure	One of 'present' (the default) or 'absent'.
 *
 *  - value	What value to set for the option.  If set to true, it will
 *		be a value-less option, e.g. 'hiddenmenu'.
 */
define bootloader::grub0::option($ensure='present', $value=undef)
{
    $qname = regexp_quote($name)

    # The Agueas Grub lens puts new options at the end of the file, after
    # all boot alternatives ('title' options).  People probably don't
    # expect that, and will be confused when trying to find such options.
    # By using ensure_line, we can put them in a reasonable position.

    if ($ensure == 'absent')
    {
	delete_lines {
	    "bootloader::grub0::option::${name}":
		file => '/boot/grub/grub.conf',
		pattern => "${qname}(=.*)?";
	}
    }
    elsif ($ensure != 'present')
    {
	fail("Bootloader::Option[${title}]: ",
	     "Bad ensure parameter, ${ensure}")
    }
    elsif ($value == true)
    {
	ensure_line {
	    "bootloader::grub0::option::${name}":
		file => '/boot/grub/grub.conf',
		line => $name,
		pattern => "${qname}(\s*=.*)",
		addhow => 'prepend',
		where => '((title\s*.*$)|)$';
	}
    }
    else
    {
	ensure_line {
	    "bootloader::grub0::option::${name}":
		file => '/boot/grub/grub.conf',
		line => "${name}=${value}",
		pattern => "${qname}(\s*=.*)",
		addhow => 'prepend',
		where => '((title\s*.*$)|)$';
	}
    }
}
