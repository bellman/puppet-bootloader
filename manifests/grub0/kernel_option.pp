# Copyright © 2017-2018   Thomas Bellman, Linköping, Sweden
# Licensed under the GNU LGPL v3+; see the README file for more information.


/*
 * Internal helper for the bootloader::kernel_option definition.
 */
define bootloader::grub0::kernel_option($ensure, $value)
{
    if ($ensure == 'absent')
    {
	augeas {
	    "bootloader::grub0::kernel_option::${name}":
		incl => '/boot/grub/grub.conf',
		lens => 'Grub.lns',
		context => '/files/boot/grub/grub.conf',
		changes => "rm title[*]/kernel/${name}";
	}
    }
    elsif ($ensure != 'present')
    {
	fail("Bootloader::Grub0::Kernel_Option[${title}]: ",
	     "Bad ensure parameter, ${ensure}")
    }
    elsif ($value == true or $value == false)
    {
	augeas {
	    "bootloader::grub0::kernel_option::${name}":
		incl => '/boot/grub/grub.conf',
		lens => 'Grub.lns',
		context => '/files/boot/grub/grub.conf',
		changes => "setm title[*]/kernel ${name}";
	}
    }
    else
    {
	augeas {
	    "bootloader::grub0::kernel_option::${name}":
		incl => '/boot/grub/grub.conf',
		lens => 'Grub.lns',
		context => '/files/boot/grub/grub.conf',
		changes => "setm title[*]/kernel ${name} '${value}'";
	}
    }
}
